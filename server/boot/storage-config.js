'use strict';


// module.exports = function(server) {
//   server.dataSources.elements.connector.getFilename = function(uploadingFile, req, res) {
//     return Math.random().toString().substr(2) + '.jpg';
//   };
// };
module.exports = function(server){
  server.dataSources.elements.connector.getFilename = function (file, req, res) {
    //file.name is original filename uploaded
    var filename = req.query.filename || 'general.ext';
    return filename;
  }
};
